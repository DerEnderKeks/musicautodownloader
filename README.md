###Dependencies
* youtube-dl
* aria2c

###Usage
####Enable auto updates

> TL;DR: Add a crontab entry for `.updater/updateall.sh` to enable auto updates

#####Example
* execute `crontab -e`
* insert `*/15 *  * * *   bash <Path>/.updater/updateall.sh > <Path>/.updater/updater.log 2>&1` to execute the script every 15 minutes and log everything to `<Path>/.updater/updater.log`
* save
* done

####Add new channels
* Execute `.template/add.sh <Name> <Playlist URL>` to add a new playlist and auto update it
