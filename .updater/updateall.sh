#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PIDFILE="$DIR/updater.pid"

if [ -f "$PIDFILE" ]; then
	echo "Already running. (PID: $(head -n1 "${PIDFILE}"))"
	exit 0
fi

echo "$$" > "$PIDFILE"

FILES="$DIR/auto/*"

for f in ${FILES}; do
	bash "$(head -n1 "$f")" &
done

rm "$PIDFILE" -f
