#!/bin/bash

PLAYLIST="PLAYLISTPLACEHOLDER"
TARGET="TARGETPLACEHOLDER"
ARCHIVE="$TARGET/.updater/downloaded.txt"
PIDFILE="$TARGET/.updater/updater.pid"
TIMESTAMPFILE="$TARGET/.updater/timestamp"

if [ -f "$PIDFILE" ]; then
	echo "Already running. (PID: $(head -n1 "${PIDFILE}"))"
	exit 0
fi

echo "$$" > "$PIDFILE"

#rm "$TIMESTAMPFILE" -f
#touch "$TIMESTAMPFILE"

/usr/local/bin/youtube-dl -x -i --download-archive "$ARCHIVE" --external-downloader aria2c --external-downloader-args '-x 8' --audio-format mp3 --audio-quality 0 -o "$TARGET/%(title)s.%(format)s" "$PLAYLIST"

#find "$TARGET/" -newer "$TIMESTAMPFILE" -name "*.mp3" -exec normalize-audio {} \; # Broken

#rm "$TIMESTAMPFILE" -f
rm "$PIDFILE" -f
