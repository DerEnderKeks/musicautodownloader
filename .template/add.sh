#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]; then
	echo "Usage: $0 <Name> <Playlist URL>"
	exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NAME="$1"
PLAYLIST="$2"
TARGET=$( readlink -f "${DIR}/../${NAME}" )
UTARGET="$TARGET/.updater"


mkdir -p "$UTARGET"
cp "$DIR/update.sh" "$UTARGET"
chmod +x "$UTARGET/update.sh"
sed -i "s*PLAYLISTPLACEHOLDER*$PLAYLIST*g" "$UTARGET/update.sh"
sed -i "s*TARGETPLACEHOLDER*$TARGET*g" "$UTARGET/update.sh"

mkdir -p "$DIR/../.updater/auto"
echo "$UTARGET/update.sh" >> "$DIR/../.updater/auto/$NAME"
